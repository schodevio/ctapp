var express = require('express');
var router = express.Router();

var wifi = require('node-wifi');

// middleware to use for all requests
router.use(function(req, res, next) {
  wifi.init({ iface : 'wlp3s0' })
  next(); // make sure we go to the next routes and don't stop here
})



// Get current network info
router.route('/self').get(function(req, res) {
  wifi.getCurrentConnections(function(err, network) {
    if (err) {
      res.json({ error: err })
    } else {
      res.json(network)
    }
  })
})

// Disconnect current network
router.route('/self').delete(function(req, res) {
  wifi.disconnect(function(err) {
    if (err) {
      res.json({ error: err })
    } else {
      res.json({ message: 'Network disconnected!' })
    }
  })
})

// List all networks
router.route('/').get(function(req, res) {
  wifi.scan(function(err, networks) {
    if (err) {
      res.json({ error: err });
    } else {
      res.json(networks);
    }
  });
})

// Connect to network
router.route('/').post(function(req, res) {

  var credentials = {
    ssid: req.body.ssid,
    password: req.body.password
  }

  wifi.connect(credentials, function(err) {
    if (err) {
      res.json({ error: err });
    } else {
      res.json({ message: 'Connected to ' + credentials['ssid'] + '!' });
    }
  });
})


module.exports = router;
