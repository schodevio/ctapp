var express = require('express');
var router = express.Router();

const { Client } = require('tplink-smarthome-api');


// middleware to use for all requests
router.use(function(req, res, next) {
  next(); // make sure we go to the next routes and don't stop here
});

router.get('/', function(req, res) {
  const client = new Client();

  client.startDiscovery().on('device-new', (device) => {
    device.getSysInfo().then(function(obj) {
      obj['host'] = device.host;
      res.json(obj);
    });
  });
});

router.post('/switch', function(req, res) {
  var ip = req.body.ip;

  const client = new Client();

  const plug = client.getDevice({host: ip}).then((device)=>{
    device.togglePowerState();
    var status = device.relayState ? 'OFF' : 'ON'; // reversed
    res.json({ msg: ip + ' Switched to ' + status });
  });
});

router.post('/alias', function(req, res) {
  var ip = req.body.ip;
  var alias = req.body.alias;

  const client = new Client();

  const plug = client.getDevice({host: ip}).then((device)=>{
    device.setAlias(alias);
    res.json({ msg: 'Name has been updated to ' + alias });
  });
});

router.post('/led', function(req, res) {
    var ip = req.body.ip;
    var ledState = req.body.led;
  
    const client = new Client();
  
    const plug = client.getDevice({host: ip}).then((device)=>{
      device.setLedState(ledState)
      res.json({ msg: 'Led has been switched' });
    });
  });

router.post('/connect', function(req, res) {
    var payload = {
        "netif": {
            "set_stainfo": {
                "ssid": req.body.ssid, // nazwa sieci domowej
                "password": req.body.password //hasło do sieci domowej
            }
        }
    }

    const client = new Client();

    const plug = client.send(payload, '192.168.0.1').then((device)=>{
        res.json({ msg: 'Plug has been paired' });
    });
    
});


module.exports = router;
