var express = require('express');
var router = express.Router();

var SerialPort = require('serialport');
var port = new SerialPort('/dev/ttyACM0', { baudRate: 9600 });

// middleware to use for all requests
router.use(function(req, res, next) {
  next(); // make sure we go to the next routes and don't stop here
})

router.post('/', function (req, res) {
  var code = req.body.code

  port.write(code, function(err) {
    var result = err ? { error: err } : { message: 'Code ' + code + ' has been sent!' };
    res.json(result);
  });
})

module.exports = router;
