// server.js

// BASE SETUP
// ============================================================================

// call the packages we need
var express    = require('express')        // call express
var api        = express()                 // define our api using express
var bodyParser = require('body-parser')

// configure api to use bodyParser()
// this will let us get the data from a POST
api.use(bodyParser.urlencoded({ extended: true }))
api.use(bodyParser.json())

var port = process.env.PORT || 62072       // set our port


api.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});

// Lights resources
var serialPortRouting = require('./api/serialPortController.js')
api.use('/api/serialport', serialPortRouting)

// Networks
var networkRouter = require('./api/networkController.js');
api.use('/api/networks', networkRouter);

// Plugs
var plugRouter = require('./api/plugController.js');
api.use('/api/plugs', plugRouter);

// START THE SERVER
// ============================================================================
api.listen(port)
console.log('API server is running on port: ' + port)
