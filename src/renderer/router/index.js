import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'welcome-view',
      component: require('@/components/WelcomeView').default
    },
    {
      path: '/desk',
      name: 'desk-controller',
      component: require('@/components/DeskControllerView').default
    },
    {
      path: '/plugs',
      name: 'plugs-controller',
      component: require('@/components/PlugsControllerView').default
    },
    {
      path: '/settings',
      name: 'settings',
      component: require('@/components/SettingsControllerView').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
